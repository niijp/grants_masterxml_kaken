# KAKENマスタデータ #

* このリポジトリでは、KAKENの最新のマスタデータをご利用頂くことができます。
* 本リポジトリでは、常に最新のマスタデータがご利用頂けます。
* それぞれのファイルの内容については、以下の通りです。

### category_master_kakenhi.xml ###

* 研究種目のマスタデータのXMLファイルです

### field_master_kakenhi.xml ###

* 研究分野のマスタデータのXMLファイルです

### institution_master_kakenhi.xml ###

* 研究機関のマスタデータのXMLファイルです

### section_master_kakenhi.xml ###

* 応募区分のマスタデータのXMLファイルです

### review_section_master_kakenhi.xml ###

* 審査区分のマスタデータのXMLファイルです

### label_master_kakenhi.xml ###

* KAKENにおける各項目の名称（ラベル）についてのマスタデータです

### code_master_kakenhi.xml ###

* 上記ラベルマスタの項目における、取り得る値の種類と、それぞれの名称についてのマスタデータです


---

# KAKEN Master Data #
* In this repository, you can use the latest master data for KAKEN.
* In this repository, you can always use the latest master data.
* The contents of each file are as follows:
### category_master_kakenhi.xml ###
* This is the XML file of the master data for Research Category.
### field_master_kakenhi.xml ###
* This is the XML file of the master data for Research Field.
### institution_master_kakenhi.xml ###
* This is the XML file of the master data for Research Institution.
### section_master_kakenhi.xml ###
* This is the XML file of the master data for Allocation Type.
### review_section_master_kakenhi.xml ###
* This is the XML file of the master data for Review Section.
### label_master_kakenhi.xml ###
* This is the master data for the names of each item (labels) in KAKEN.
### code_master_kakenhi.xml ###
* This is the master data for the types of potential values and each name for the items in the above label master.
